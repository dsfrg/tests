﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.Services;
using Common.DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.App.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserRoleController : ControllerBase
    {
        private readonly UserRoleService _service;

        public UserRoleController(UserRoleService service)
        {
            _service = service;
        }

        [HttpGet]
        public ActionResult<IEnumerable<UserRoleDto>> GetAllRoless()
        {
            return Ok(_service.GetUserRoles());
        }

        [HttpGet("{id}")]
        public ActionResult<UserRoleDto> GetSingleUserRole(int id)
        {
            return Ok(_service.GetUserRole(id));
        }


        [HttpPost]
        public IActionResult AddSingleRole(UserRoleDto role)
        {
            _service.AddRole(role);
            return Ok();
        }

        [HttpPost("range")]
        public IActionResult AddUsers(IEnumerable<UserRoleDto> roles)
        {
            _service.AddRangeOfRoles(roles);
            return Ok();
        }

        [HttpDelete]
        public IActionResult DeleteRole(UserRoleDto role)
        {
            _service.Delete(role);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteRole(int id)
        {
            _service.DeleteById(id);
            return NoContent();
        }


        [HttpPut]
        public IActionResult ChangeRole(UserRoleDto role)
        {
            _service.Update(role);
            return Ok();
        }
    }
}
