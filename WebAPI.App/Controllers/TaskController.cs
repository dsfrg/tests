﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.Services;
using Common.DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.App.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private readonly TaskService _taskService;

        public TaskController(TaskService taskService)
        {
            _taskService = taskService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<TaskDto>> GetAllTasks()
        {
            return Ok(_taskService.GetTasks());
        }

        [HttpGet("{id}")]
        public ActionResult<TaskDto> GetSingleTask(int id)
        {
            return Ok(_taskService.GetTask(id));
        }


        [HttpPost]
        public IActionResult AddSingleTAsk(TaskDto task)
        {
            _taskService.AddTask(task);
            return Ok();
        }

        [HttpPost("range")]
        public IActionResult AddTasks(IEnumerable<TaskDto> tasks)
        {
            _taskService.AddRangeOfTasks(tasks);
            return Ok();
        }

        [HttpDelete]
        public IActionResult DeleteTask(TaskDto task)
        {
            _taskService.Delete(task);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteTaskById(int id)
        {
            _taskService.DeleteById(id);
            return NoContent();
        }


        [HttpPut]
        public IActionResult ChangeTask(TaskDto task)
        {
            _taskService.Update(task);
            return Ok();
        }

        [HttpGet("AllUnfinishedTask/{userId}")]
        public ActionResult<IEnumerable<TaskDto>> GetUnfinishedTasksByUserId(int userId)
        {
            return Ok(_taskService.GetUnfinishedTaskByUserId(userId));
        }
    }
}
