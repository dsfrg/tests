﻿using Common.DTOs;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using System.Linq;
using System;
using WebAPI.App;

namespace IntegrationsTests
{
    public class IntegrationsTests: IClassFixture<CustomWebFactory<Startup>>
    {
        private HttpClient client;
        const string URL = @"http://localhost:56122/api/";
        public IntegrationsTests(CustomWebFactory<Startup> factory)
        {
            client = factory.CreateClient();
        }

        [Fact]
        public async Task AddProject_ReturnOk()
        {
            var projectDto = new ProjectDto()
            {
                Name = "test",
                Description = "Test",
                CreatedAt = DateTime.Now,
                Deadline = DateTime.Now,
                AuthorId = 2,
                TeamId = 2
            };

            string json = JsonConvert.SerializeObject(projectDto);
            var response = await client.PostAsync(URL + "project", new StringContent(json, Encoding.UTF8, "application/json"));

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

        [Fact]
        public async Task AddProject_WhenProjectExist_ReturnOk()
        {
            var projectDto = new ProjectDto()
            {
                Id = 5,
                Name = "test",
                Description = "Test",
                CreatedAt = DateTime.Now,
                Deadline = DateTime.Now,
                AuthorId = 2,
                TeamId = 2
            };

            string json = JsonConvert.SerializeObject(projectDto);
            var response = await client.PostAsync(URL + "project", new StringContent(json, Encoding.UTF8, "application/json"));

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

        [Fact]
        public async Task AddProject_WhenDataIsNull_ReturnInternalServerError()
        {
            ProjectDto projectDto = null;

            string json = JsonConvert.SerializeObject(projectDto);
            var response = await client.PostAsync(URL + "project", new StringContent(json, Encoding.UTF8, "application/json"));

            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task DeleteUserById_ReturnNoContent()
        {
            int userId = 1;

            var response = await client.DeleteAsync(URL + $"user/{userId}");

            Assert.Equal(HttpStatusCode.NoContent, response.StatusCode);
        }

        [Fact]
        public async Task DeleteUserById_WhenUserDoesNotExist_ReturnInternalServerError()
        {
            int userId = 100;

            var response = await client.DeleteAsync(URL + $"user/{userId}");

            Assert.Equal(HttpStatusCode.InternalServerError, response.StatusCode);
        }

        [Fact]
        public async Task DeleteUserById_WhenUserISNull_ReturnInternalBadRequest()
        {
            UserDto userDto = null;

            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Delete,
                RequestUri = new Uri(URL + "user"),
                Content = new StringContent(JsonConvert.SerializeObject(userDto), Encoding.UTF8, "application/json")
            };
            var response = await client.SendAsync(request);

            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task CreateTeam_ReturnOk()
        {
            var teamDto = new TeamDto()
            { 
                Name = "MyTeam",
                CreatedAt = DateTime.Now
            };


            string json = JsonConvert.SerializeObject(teamDto);
            var response = await client.PostAsync(URL + "team", new StringContent(json, Encoding.UTF8, "application/json"));

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

        [Fact]
        public async Task CreateUser_ReturnOk()
        {
            var userDto = new UserDto()
            {
                FirstName = "Vasyl",
                LastName = "Tytysh",
                EmailAddress = "vasyl@gmail.com",
                TeamId = 2
            };

            string json = JsonConvert.SerializeObject(userDto);
            var response = await client.PostAsync(URL + "user", new StringContent(json, Encoding.UTF8, "application/json"));

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

        [Fact]
        public async Task DeleteTask_ReturnNoContent()
        {
            int taskId = 1;

            var response = await client.DeleteAsync(URL + $"task/{taskId}");

            Assert.Equal(HttpStatusCode.NoContent, response.StatusCode);
        }

        [Fact]
        public async Task DeleteTask_WhenDoesNotExist_ReturnInternalServerError()
        {
            int taskId = 300;

            var response = await client.DeleteAsync(URL + $"task/{taskId}");

            Assert.Equal(HttpStatusCode.InternalServerError, response.StatusCode);
        }

        [Fact]
        public async Task DeleteTaskByObject_WhenDoesNotExist_ReturnInternalServerError()
        {
            TaskDto userDto = null;

            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Delete,
                RequestUri = new Uri(URL + "task"),
                Content = new StringContent(JsonConvert.SerializeObject(userDto), Encoding.UTF8, "application/json")
            };
            var response = await client.SendAsync(request);

            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Theory]
        [InlineData(1,2)]
        [InlineData(5,1)]
        [InlineData(7,6)]
        [InlineData(45,2)]
        public async Task GetAllUnfinishedTasks_ReturnCorrectValues(int userId, int expectedCount)
        {
            string api = $"task/AllUnfinishedTask/{userId}";

            var response = await client.GetAsync(URL + api);
            var listOfResults = JsonConvert.DeserializeObject<IEnumerable<TaskDto>>(await response.Content.ReadAsStringAsync());

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.Equal(listOfResults.Count(), expectedCount);
        }
    }
}
