﻿using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using DAL.Context;

namespace IntegrationsTests
{
    public class CustomWebFactory<T>: WebApplicationFactory<T>
        where T: class
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)//https://docs.microsoft.com/uk-ua/aspnet/core/test/integration-tests?view=aspnetcore-3.1
        {
            builder.ConfigureServices(services =>
            {
                var descriptor = services.SingleOrDefault(
                    d => d.ServiceType ==
                        typeof(DbContextOptions<InitialDataContext>));

                if (descriptor != null)
                {
                    services.Remove(descriptor);
                }

                services.AddDbContext<InitialDataContext>(options =>
                {
                    options.UseInMemoryDatabase("InMemoryDbForTesting");
                });

                var sp = services.BuildServiceProvider();

                using (var scope = sp.CreateScope())
                {
                    var scopedServices = scope.ServiceProvider;
                    var db = scopedServices.GetRequiredService<InitialDataContext>();
                    var logger = scopedServices
                        .GetRequiredService<ILogger<CustomWebFactory<T>>>();

                    db.Database.EnsureCreated();

                }
            });
        }
    }
}
