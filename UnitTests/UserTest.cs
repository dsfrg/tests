﻿using AutoMapper;
using BLL.Profiles;
using BLL.Services;
using Common.DTOs;
using DAL.Entities;
using DAL.Repository;
using FakeItEasy;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace UnitTests
{
    public class UserTest
    {
        private UserService userService;
        private IRepository<User> userRepo;
        private IMapper mapper;

        public UserTest()
        {
            userRepo = A.Fake<IRepository<User>>();
            mapper = CreateMapperConfiguration().CreateMapper();
            userService = new UserService(userRepo, mapper);
        }

        public MapperConfiguration CreateMapperConfiguration()
        {
            return new MapperConfiguration(mc =>
            {
                mc.AddProfile(new ProjectProfile());
                mc.AddProfile(new TaskProfile());
                mc.AddProfile(new TeamProfile());
                mc.AddProfile(new UserProfile());
                mc.AddProfile(new HierarhyDataProfile());
                mc.AddProfile(new UserRoleProfile());
            });
        }

        [Fact]
        public void CreateAndAddUserToDb_MustHaveHappendOnlyOnce()
        {
            var userDto = new UserDto() 
            { 
                FirstName = "Vasyl",
                LastName = "Tytysh",
                EmailAddress = "vasyl@gmail.com",
                TeamId = 2
            };

            userService.AddUser(userDto);

            A.CallTo(() => userRepo.Add(A<User>.That.Matches(x => x.FirstName == userDto.FirstName && x.LastName == userDto.LastName)))
                .MustHaveHappenedOnceExactly();
        }

        [Fact]
        public void AddUserToTheTeam_MustHaveHappendOnlyOnce()
        {
            var userDto = new UserDto()
            {
                Id = 5,
                TeamId = 4
            };

            userService.Update(userDto);

            A.CallTo(() => userRepo.Update(A<User>.That.Matches(x => x.Id == userDto.Id))).MustHaveHappenedOnceExactly();
        }

        [Fact]
        public void AddUserToTheTeam_WhenUserDoNotExists_HaveToThrowArgumentExeption()
        {
            var userDto = new UserDto()
            {
                Id = 300,
                TeamId = 4
            };

            userService.Update(userDto);

            A.CallTo(() => userRepo.Update(A<User>.That.Matches(x => x.Id == userDto.Id))).Throws<ArgumentException>();
        }
    }
}
