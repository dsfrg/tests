﻿using AutoMapper;
using BLL.Profiles;
using BLL.Services;
using Common.DTOs;
using DAL.Entities;
using DAL.Repository;
using FakeItEasy;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace UnitTests
{
    public class TaskTest
    {
        private TaskService taskService;
        private IRepository<Task> taskRepo;
        private IMapper mapper;

        public TaskTest()
        {
            taskRepo = A.Fake<IRepository<Task>>();
            mapper = CreateMapperConfiguration().CreateMapper();
            taskService = new TaskService(taskRepo, mapper);
        }
        public MapperConfiguration CreateMapperConfiguration()
        {
            return new MapperConfiguration(mc =>
            {
                mc.AddProfile(new ProjectProfile());
                mc.AddProfile(new TaskProfile());
                mc.AddProfile(new TeamProfile());
                mc.AddProfile(new UserProfile());
                mc.AddProfile(new HierarhyDataProfile());
                mc.AddProfile(new UserRoleProfile());
            });
        }
        [Fact]
        public void ChangeTaskStatusOnlyOnce_MustHaveHappendOnlyOnce()
        {
            var taskDto = new TaskDto()
            {
                Id = 100,
                State = TaskStateDto.Finished
            };

            taskService.Update(taskDto);

            A.CallTo(() => taskRepo.Update(A<Task>.That.Matches(x => x.Id == taskDto.Id))).MustHaveHappenedOnceExactly();
        }


        [Fact]
        public void ChangeTaskStatus_WhenTaskDoNotExists_HaveToThrowArgumentExeption()
        {
            var taskDto = new TaskDto()
            {
                Id = 100,
                State = TaskStateDto.Finished
            };

            taskService.Update(taskDto);

            A.CallTo(() => taskRepo.Update(A<Task>.That.Matches(x => x.Id == taskDto.Id))).Throws<ArgumentException>();
        }

        [Fact]
        public void ChangeTaskStatus_WhenTaskIsNull_HaveToThrowArgumentNullExeption()
        {
            TaskDto taskDto = null;

            taskService.Update(taskDto);

            A.CallTo(() => taskRepo.Update(A<Task>.That.Matches(x => x.Id == taskDto.Id))).Throws<ArgumentNullException>();
        }
    }
}
