﻿using AutoMapper;
using BLL.Profiles;
using BLL.Services;
using DAL.Context;
using DAL.Entities;
using DAL.Repository;
using FakeItEasy;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using Xunit.Sdk;

namespace UnitTests
{
    public class QueryTests
    {
        private QueryService _queryService;
        private readonly IMapper mapper;
        private readonly InitialDataContext context;

        public QueryTests()
        {
            context = new InitialDataContext(CreateNewContextOptions());
            mapper = CreateMapperConfiguration().CreateMapper();
            _queryService = GetQueryService();

        }

        public QueryService GetQueryService()
        {

            var teams = (JsonConvert.DeserializeObject<IEnumerable<Team>>(DataClass.teamsJson));
            var users = (JsonConvert.DeserializeObject<IEnumerable<User>>(DataClass.usersJson));
            var projects = (JsonConvert.DeserializeObject<IEnumerable<Project>>(DataClass.projectJson));
            var tasks = (JsonConvert.DeserializeObject<IEnumerable<Task>>(DataClass.tasksJson));

            var random = new Random();
            foreach (var i in users)
            {
                i.RoleId = random.Next(1, 5);
            }

            context.Projects.AddRange(projects);
            context.Users.AddRange(users);
            context.Teams.AddRange(teams);
            context.Tasks.AddRange(tasks);
            context.SaveChanges();

            var projectMock = new Mock<IRepository<Project>>();
            var userMock = new Mock<IRepository<User>>();
            var taskMock = new Mock<IRepository<Task>>();
            var teamMock = new Mock<IRepository<Team>>();

            projectMock.Setup(t => t.GetItemsQueryable()).Returns(context.Projects);
            userMock.Setup(t => t.GetItemsQueryable()).Returns(context.Users);
            taskMock.Setup(t => t.GetItemsQueryable()).Returns(context.Tasks);
            teamMock.Setup(t => t.GetItemsQueryable()).Returns(context.Teams);

            projectMock.Setup(t => t.GetItems()).Returns(context.Projects);
            userMock.Setup(t => t.GetItems()).Returns(context.Users);
            taskMock.Setup(t => t.GetItems()).Returns(context.Tasks);
            teamMock.Setup(t => t.GetItems()).Returns(context.Teams);


            return new QueryService(projectMock.Object, userMock.Object, teamMock.Object, taskMock.Object, mapper);
        }

        public MapperConfiguration  CreateMapperConfiguration()
        {
            return  new MapperConfiguration(mc =>
            {
                mc.AddProfile(new ProjectProfile());
                mc.AddProfile(new TaskProfile());
                mc.AddProfile(new TeamProfile());
                mc.AddProfile(new UserProfile());
                mc.AddProfile(new HierarhyDataProfile());
                mc.AddProfile(new UserRoleProfile());
            });
        }

        private static DbContextOptions<InitialDataContext> CreateNewContextOptions()
        {
            var serviceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();

            var builder = new DbContextOptionsBuilder<InitialDataContext>();
            builder.UseInMemoryDatabase(Guid.NewGuid().ToString())
                   .UseInternalServiceProvider(serviceProvider);

            return builder.Options;
        }


        [Theory]
        [InlineData(7,4,4)]
        [InlineData(1,1,1)]
        [InlineData(5,0, null)]
        public void GetObjectWithProjectCreatedByUserAndProjectTasks_ReturnCorrectValues(int userId, int countOfItems, int? countOfTasks)
        {
            var b = _queryService.GetDictionaryProjectToTaskCountByUserId(userId);

            Assert.Equal(countOfItems, b.Count());
            Assert.Equal(countOfTasks, b.FirstOrDefault()?.CountOfTasks);
        }

        [Theory]
        [InlineData(7, 3)]
        [InlineData(3, 2)]
        [InlineData(98, 0)]
        public void GetTasksWherePerformerIdEqualUserIdAndTaskNameLessThan45_ReturnCorrectValue(int userId, int countOfTasks)
        {
            var a = _queryService.GetTasksByUserIdWhereNameLessThan45(userId);

            Assert.Equal(a.Count(), countOfTasks);
        }

        [Theory]
        [InlineData(1,1)]
        [InlineData(7,1)]
        [InlineData(6,0)]
        public void GetFromCollectionOfTasksWhichAreFinishedIn2022ByUserId_ReturnCorrectValue(int userId, int countOfTasks)
        {
            var a = _queryService.GetFromCollectionOfTasksWhichAreFinished(userId);

            Assert.Equal(a.Count(), countOfTasks);
        }

        [Fact]
        public void GetTeamsWhereAllMembersAreOlderThan10Years_ReturnCorrectResult()
        {
            var expectedCountOfTaams = 7;
            var expectedUsersCountInFirstTeam = 3;
            var expectedUsersCountInLastTeam = 5;

            var a = _queryService.GetTeamsOlderThan10Years();
            var actualCountountOfTeams = a.Count();
            
            Assert.Equal(actualCountountOfTeams, expectedCountOfTaams);
            Assert.Equal(a.First().Users.Count(), expectedUsersCountInFirstTeam);
            Assert.Equal(a.Last().Users.Count(), expectedUsersCountInLastTeam);
        }

        [Fact]
        public void GetListOfUsersSortedByWithTasksSortedByNameLength_ReturnCorrectResult()
        {
            context.Tasks.RemoveRange(context.Tasks);
            context.SaveChanges();
            context.Users.RemoveRange(context.Users);
            context.SaveChanges();
            context.Projects.RemoveRange(context.Projects);
            context.SaveChanges();

            context.Projects.AddRange(new List<Project>()
            {
                new Project() { Name = "A", Description ="A", AuthorId = 1, TeamId = 1},
                new Project() { Name = "AB", Description ="B", AuthorId = 2, TeamId = 1 }
            });

            context.Tasks.AddRange(new List<Task>()
            {
                new Task() { Name = "A", Description ="A", ProjectId = 1, PerformerId = 1},
                new Task() { Name = "Bnnnn", Description ="B", ProjectId = 2, PerformerId = 1 },
                new Task() { Name = "C", Description ="C", ProjectId = 2, PerformerId = 2 }
            });
            context.SaveChanges();
            context.Users.AddRange(new List<User>()
            {
                new User() { FirstName = "A", LastName ="A", TeamId = 1},
                new User() { FirstName = "B", LastName ="B", TeamId = 1}
            });
            context.SaveChanges();

            var a = _queryService.GetListOfUsers();

            Assert.Equal(a.Count(), 2);
            Assert.Equal(a.First().Tasks.Count(), 2);
            Assert.Equal(a.Last().Tasks.Count(), 1);
            Assert.Equal(a.First().Tasks.First().Name, "Bnnnn");
            Assert.Equal(a.First().User.FirstName, "A");
        }

        [Fact]
        public void GetInfoAboutUser_ReturnCorrectStructure()
        {
            var expectedUserFirstName = "A";
            var expectedLongestTaskName = "C";
            var expectedLastProjectName = "LAST";
            var expectedCountOfUnfinishedTasks = 2;
            var expectedCountOfTasks = 3;

            context.Tasks.RemoveRange(context.Tasks);
            context.SaveChanges();
            context.Users.RemoveRange(context.Users);
            context.SaveChanges();
            context.Projects.RemoveRange(context.Projects);
            context.SaveChanges();

            context.Projects.AddRange(new List<Project>()
            {
                new Project() { Name = "A", Description ="A", AuthorId = 1, TeamId = 1 , CreatedAt = new DateTime(2010,10,10)},
                new Project() { Name = "LAST", Description ="A", AuthorId = 1, TeamId = 1, CreatedAt = new DateTime(2020, 10,10) }
            });
            context.SaveChanges();

            context.Tasks.AddRange(new List<Task>()
            {
                new Task() { Name = "A", Description ="A", ProjectId = 1, PerformerId = 1, State = TaskState.Canceled},
                new Task() { Name = "Bnnnn", Description ="B", ProjectId = 1, PerformerId = 1, State = TaskState.Finished },
                new Task() { Name = "C", Description ="C", ProjectId = 1, PerformerId = 1, State = TaskState.Canceled,
                    CreatedAt = DateTime.Now.AddYears(-20),
                    FinishedAt = DateTime.Now}
            });
            context.SaveChanges();
            context.Users.AddRange(new List<User>()
            {
                new User() { FirstName = "A", LastName ="A", TeamId = 1},
            });
            context.SaveChanges();

            var a = _queryService.GetInfoAboutUser(1);

            Assert.Equal(a.User.FirstName, expectedUserFirstName);
            Assert.Equal(a.CountOfUnfinishedOrCanceledTasks, expectedCountOfUnfinishedTasks);
            Assert.Equal(a.CountOfTasks, expectedCountOfTasks);
            Assert.Equal(a.LongestTask.Name, expectedLongestTaskName);
            Assert.Equal(a.LastProject.Name, expectedLastProjectName);
        }

        [Fact]
        public void GetInfoAboutProject_ReturnCorrectStructure()
        {
            var expectedProjectName = "A";
            var expectedTaskDescription = "A";
            var expectedTaskName = "C";
            var expectedCountOfUsers = 1;

            context.Tasks.RemoveRange(context.Tasks);
            context.SaveChanges();
            context.Users.RemoveRange(context.Users);
            context.SaveChanges();
            context.Projects.RemoveRange(context.Projects);
            context.SaveChanges();

            context.Projects.AddRange(new List<Project>()
            {
                new Project() { Name = "A", Description ="A", AuthorId = 1, TeamId = 1 , CreatedAt = new DateTime(2010,10,10)}
            });
            context.SaveChanges();

            context.Tasks.AddRange(new List<Task>()
            {
                new Task() { Name = "A", Description ="A", ProjectId = 1, PerformerId = 1},
                new Task() { Name = "B", Description ="B", ProjectId = 1, PerformerId = 1},
                new Task() { Name = "C", Description ="C", ProjectId = 1, PerformerId = 1}
            });
            context.SaveChanges();
            context.Users.AddRange(new List<User>()
            {
                new User() { FirstName = "A", LastName ="A", TeamId = 1},
            });
            context.SaveChanges();

            var a = _queryService.GetProjectInfo().First();

            Assert.Equal(a.Project.Name, expectedProjectName);
            Assert.Equal(a.LongestTaskByDescription.Description, expectedTaskDescription);
            Assert.Equal(a.ShortestTaskByName.Name, expectedTaskName);
            Assert.Equal(a.CountOfUsers, expectedCountOfUsers);
        }
    }
}
