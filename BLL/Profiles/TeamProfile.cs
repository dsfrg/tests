﻿using Common.DTOs;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Profiles
{
    public class TeamProfile: AutoMapper.Profile
    {
        public TeamProfile()
        {
            CreateMap<Team, TeamDto>().ReverseMap();
        }
    }
}
