﻿using AutoMapper;
using Common.DTOs;
using DAL.Entities;
using DAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL.Services
{
    public class TaskService
    {
        private readonly IRepository<Task> _taskRepo;
        private readonly IMapper _mapper;

        public TaskService(IRepository<Task> repo, IMapper mapper)
        {
            _taskRepo = repo;
            _mapper = mapper;
        }

        public void AddTask(TaskDto item)
        {
            _taskRepo.Add(_mapper.Map<Task>(item));
        }

        public void AddRangeOfTasks(IEnumerable<TaskDto> range)
        {
            _taskRepo.AddRange(_mapper.Map<IEnumerable<Task>>(range));
        }

        public void Delete(TaskDto item)
        {
            _taskRepo.Delete(_mapper.Map<Task>(item));
        }

        public void DeleteById(int id)
        {
            _taskRepo.DeleteById(id);
        }

        public TaskDto GetTask(int id)
        {
            return _mapper.Map<TaskDto>(_taskRepo.GetItem(id));
        }

        public IEnumerable<TaskDto> GetTasks()
        {
            return _mapper.Map<IEnumerable<TaskDto>>(_taskRepo.GetItems());
        }

        public void Update(TaskDto item)
        {
            _taskRepo.Update(_mapper.Map<Task>(item));
        }

        public IEnumerable<TaskDto> GetUnfinishedTaskByUserId(int userId)
        {
            return _mapper.Map<IEnumerable<TaskDto>>(_taskRepo.GetItems().Where(x => x.PerformerId == userId && x.State != TaskState.Finished));
        }
    }
}
