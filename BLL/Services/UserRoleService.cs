﻿using AutoMapper;
using Common.DTOs;
using DAL.Entities;
using DAL.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Services
{
    public class UserRoleService
    {
        private readonly IRepository<UserRole> _repository;
        private readonly IMapper _mapper;

        public UserRoleService(IRepository<UserRole> repo, IMapper mapper)
        {
            _repository = repo;
            _mapper = mapper;
        }

        public void AddRole(UserRoleDto item)
        {
            _repository.Add(_mapper.Map<UserRole>(item));
        }

        public void AddRangeOfRoles(IEnumerable<UserRoleDto> range)
        {
            _repository.AddRange(_mapper.Map<IEnumerable<UserRole>>(range));
        }

        public void Delete(UserRoleDto item)
        {
            _repository.Delete(_mapper.Map<UserRole>(item));
        }

        public void DeleteById(int id)
        {
            _repository.DeleteById(id);
        }

        public UserRoleDto GetUserRole(int id)
        {
            return _mapper.Map<UserRoleDto>(_repository.GetItem(id));
        }

        public IEnumerable<UserRoleDto> GetUserRoles()
        {
            return _mapper.Map<IEnumerable<UserRoleDto>>(_repository.GetItems());
        }

        public void Update(UserRoleDto item)
        {
            _repository.Update(_mapper.Map<UserRole>(item));
        }
    }
}
