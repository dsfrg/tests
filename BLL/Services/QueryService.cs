﻿using AutoMapper;
using Common.DTOs;
using Common.DTOs.QueryDtos;
using DAL.Context;
using DAL.Entities;
using DAL.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BLL.Services
{
    public class QueryService
    {
        private readonly IRepository<Project> _projectRepo;
        private readonly IRepository<User> _userRepo;
        private readonly IRepository<Team> _teamRepo;
        private readonly IRepository<Task> _taskRepo;
        private readonly IMapper _mapper;


        public QueryService(
            IRepository<Project> projectRepo,
            IRepository<User> userRepo,
            IRepository<Team> teamRepo,
            IRepository<Task> taskRepo,
            IMapper mapper
            )
        {
            _projectRepo = projectRepo;
            _userRepo = userRepo;
            _teamRepo = teamRepo;
            _taskRepo = taskRepo;
            _mapper = mapper;
        }

        public IEnumerable<HierarchyDataDto> GetCombinedDataStructureDto()
        {
            return _mapper.Map<IEnumerable<HierarchyDataDto>>(GetCombinedDataStructure());
        }
        public IEnumerable<HierarchyData> GetCombinedDataStructure() //create a hierarchical data structure 
        {
            //_taskRepo.GetItemsQueryable().FromSql("")

            //var a = from task in _taskRepo.GetItemsQueryable()
            //        join project in (
            //                    from project in _projectRepo.GetItemsQueryable()
            //                    join user in _userRepo.GetItemsQueryable() on project.AuthorId equals user.Id
            //                    join team in _teamRepo.GetItemsQueryable() on project.TeamId equals team.Id
            //                    select project
            //                    )
            //on task.ProjectId equals project.Id into bars
            //select bars;
            
            return _taskRepo.GetItemsQueryable()
                        .Include(x => x.Performer)
                        .Include(x => x.Project)
                            .ThenInclude(x => x.Author)
                            .ThenInclude(x => x.Team)
                        .AsEnumerable()
                        .GroupBy(x => x.Project)
                        .Select(x => new HierarchyData() {
                            Id = x.Key.Id,
                            Name = x.Key.Name,
                            Description = x.Key.Description,
                            CreatedAt = x.Key.CreatedAt,
                            Deadline = x.Key.Deadline,
                            AuthorId = x.Key.AuthorId,
                            Author = x.Key.Author,
                            TeamId = x.Key.TeamId,
                            Team = x.Key.Team,
                            Tasks = x.ToList()
                        });
        }
        public IEnumerable<ProjectToTaskCountStructureDto> GetDictionaryProjectToTaskCountByUserId(int userId)
        {

            return GetCombinedDataStructureDto().Where(p => p.AuthorId == userId)
                .Select(p => new ProjectToTaskCountStructureDto()
                { 
                    Project = _mapper.Map<ProjectDto>(p),
                    CountOfTasks = p.Tasks.Count
                });
        }

        public IEnumerable<TaskDto> GetTasksByUserIdWhereNameLessThan45(int userId)
        {
            return GetCombinedDataStructureDto().SelectMany(x => x.Tasks)
                .Where(p => p.PerformerId == userId && p.Name.Length < 45);
        }

        public IEnumerable<TasksIdNameDto> GetFromCollectionOfTasksWhichAreFinished(int userId)
        {
            return GetCombinedDataStructureDto().SelectMany(x => x.Tasks)
                    .Where(p => p.PerformerId == userId && p.State == TaskStateDto.Finished && p.FinishedAt.Year == 2020)
                    .Select(x => new TasksIdNameDto()
                    {
                        Id = x.Id,
                        Name = x.Name
                    });
        }

        public IEnumerable<TeamUsersDto> GetTeamsOlderThan10Years()
        {
            return _userRepo.GetItemsQueryable()
                .Include(x => x.Team)
                .OrderByDescending(x => x.RegisteredAt)
                .AsEnumerable()
                .GroupBy(x => x.Team)
                .Where(x => x.Key != null) // filter users which do not have team
                .Where(x => x.All(p => (DateTime.Now.Year - p.BirthdayDate.Year) > 10))
                .Select(x => new TeamUsersDto()
                {
                    TeamId = x.Key.Id,
                    TeamName = x.Key.Name,
                    Users = _mapper.Map<IEnumerable<UserDto>>(x.ToList())
                });
        }

        public IEnumerable<UserTasksDto> GetListOfUsers()
        {
            var a = GetCombinedDataStructure().ToList();
            return GetCombinedDataStructure()
                .SelectMany(x => x.Tasks)
                .OrderBy(x => x.Performer.FirstName)
                .GroupBy(x => x.Performer)
                .Select(x => new UserTasksDto() { 
                    User = _mapper.Map<UserDto>(x.Key),
                    Tasks = _mapper.Map<IEnumerable<TaskDto>>(x.OrderByDescending(x => x.Name.Length))
                });
        }

        public CombinedInfoAboutUserTask6Dto GetInfoAboutUser(int userId)
        {
            return GetCombinedDataStructureDto()
                .Where(x => x.AuthorId == userId)
                .Select(x => new CombinedInfoAboutUserTask6Dto()
                {
                    User = x.Author,
                    LastProject = _mapper.Map<ProjectDto>(_projectRepo.GetItemsQueryable()
                                                        .Where(y => y.AuthorId == userId)
                                                        .OrderByDescending(y => y.CreatedAt)
                                                        .FirstOrDefault()),
                    CountOfTasks = _taskRepo.GetItems()
                                    .Count(t => t.PerformerId == x.AuthorId),
                    CountOfUnfinishedOrCanceledTasks = _taskRepo.GetItems().Count(t => t.State != TaskState.Finished && t.PerformerId == x.AuthorId),
                    LongestTask = _mapper.Map<TaskDto>(_taskRepo.GetItems()
                                                        .Where(t => t.PerformerId == x.AuthorId)
                                                        .OrderByDescending(t => t.FinishedAt - t.CreatedAt)
                                                        .FirstOrDefault())
                })
                .FirstOrDefault();
        }

        public IEnumerable<CombinedInfoAboutProjectTask7Dto> GetProjectInfo()
        {
            return _taskRepo.GetItemsQueryable()
                .Include(x => x.Performer)
                .Include(x => x.Project)
                .AsEnumerable()
                .GroupBy(x => x.Project)
                .Select(x => new CombinedInfoAboutProjectTask7Dto()
                {
                    Project = _mapper.Map<ProjectDto>(x.Key),
                    LongestTaskByDescription = _mapper.Map<TaskDto>(GetCombinedDataStructure().SelectMany(x => x.Tasks)
                                            .Where(t => t.ProjectId == x.Key.Id)
                                            .OrderBy(t => t.Description)
                                            .First()),
                    ShortestTaskByName = _mapper.Map<TaskDto>(GetCombinedDataStructure().SelectMany(x => x.Tasks)
                                            .Where(t => t.ProjectId == x.Key.Id)
                                            .OrderBy(t => t.Name).Last()),
                    CountOfUsers = _userRepo.GetItemsQueryable()
                                    .Count(u => u.TeamId == x.Key.TeamId && (_taskRepo.GetItemsQueryable()
                                                                            .Count(q => q.ProjectId == x.Key.Id) <= 3 || x.Key.Description.Length > 20))
                });
        }
    }
}
