﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.QueryDtos
{
    public class TeamUsersDto
    {
        public int TeamId { get; set; }
        public string TeamName { get; set; }
        public IEnumerable<UserDto> Users { get; set; }
    }
}
