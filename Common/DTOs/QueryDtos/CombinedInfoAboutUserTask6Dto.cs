﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.QueryDtos
{
    public class CombinedInfoAboutUserTask6Dto
    {
        public UserDto User { get; set; }
        public ProjectDto LastProject { get; set; }
        public int CountOfTasks { get; set; }
        public int CountOfUnfinishedOrCanceledTasks { get; set; }
        public TaskDto LongestTask { get; set; }
    }
}
