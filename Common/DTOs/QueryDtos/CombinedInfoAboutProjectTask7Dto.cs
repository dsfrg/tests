﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.QueryDtos
{
    public class CombinedInfoAboutProjectTask7Dto
    {
        public ProjectDto Project { get; set; }
        public TaskDto LongestTaskByDescription { get; set; }
        public TaskDto ShortestTaskByName { get; set; }
        public int CountOfUsers { get; set; }
    }
}
