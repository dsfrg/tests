﻿using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;

namespace DAL.Context
{
    public static class InitialDataContextExtension
    {
        public static string ReadDataFromJson(string path)
        {
            using (StreamReader reader = new StreamReader(path))
            {
                var a = reader.ReadToEnd();
                return a;
            }
        }
        public async static System.Threading.Tasks.Task<string> GetStringResponce(string path)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                var responseBody =  await httpClient.GetAsync(path);
                
                return  await responseBody.Content.ReadAsStringAsync();
            }
        }
        public static void Seed(this ModelBuilder modelBuilder)
        {

            var teams = new List<Team>(JsonConvert.DeserializeObject<IEnumerable<Team>>(GetStringResponce(@"https://res.cloudinary.com/dgha1tqtm/raw/upload/v1595276110/Teams_iaqhqa.json").Result));
            var users = new List<User>(JsonConvert.DeserializeObject<IEnumerable<User>>(GetStringResponce(@"https://res.cloudinary.com/dgha1tqtm/raw/upload/v1595276110/Users_emk4u9.json").Result));
            var projects = new List<Project>(JsonConvert.DeserializeObject<IEnumerable<Project>>(GetStringResponce(@"https://res.cloudinary.com/dgha1tqtm/raw/upload/v1595276110/Projects_lnxvqs.json").Result));
            var tasks = new List<Task>(JsonConvert.DeserializeObject<IEnumerable<Task>>(GetStringResponce(@"https://res.cloudinary.com/dgha1tqtm/raw/upload/v1595276110/Tasks_aqeiko.json").Result));

            var random = new Random();
            foreach (var user in users)
            {
                user.RoleId = random.Next(1,5);
            }
            var taskStateModels = new List<TaskStateModel>()
            { 
                new TaskStateModel()
                {
                    Id = 1,
                    Value = "Created"
                },
                new TaskStateModel()
                {
                    Id = 2,
                    Value = "Started"
                },

                new TaskStateModel()
                {
                    Id = 3,
                    Value = "Finished"
                },
                new TaskStateModel()
                {
                    Id = 4,
                    Value = "Canceled"
                }

            };
            var roles = new List<UserRole>()
            {
                new UserRole()
                {
                    Id = 1,
                    Role = "Developer"
                },

                new UserRole()
                {
                    Id = 2,
                    Role = "QA"
                },

                new UserRole()
                {
                    Id = 3,
                    Role = "DevOps"
                },

                new UserRole()
                {
                    Id = 4,
                    Role = "PM"
                }
            };

            modelBuilder.Entity<User>().HasData(users);
            modelBuilder.Entity<Task>().HasData(tasks);
            modelBuilder.Entity<Project>().HasData(projects);
            modelBuilder.Entity<Team>().HasData(teams);
            modelBuilder.Entity<TaskStateModel>().HasData(taskStateModels);
            modelBuilder.Entity<UserRole>().HasData(roles);
        }
    }
}
