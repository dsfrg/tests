﻿using DAL.Context;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Repository
{
    public class UserRoleRepository: IRepository<UserRole>
    {
        private readonly InitialDataContext _context;

        public UserRoleRepository(InitialDataContext context)
        {
            _context = context;
        }
        public UserRole GetItem(int id)
        {
            var role = _context.UserRoles.FirstOrDefault(x => x.Id == id);
            if (role == null)
                throw new ArgumentException("user");

            return role;
        }

        public void Add(UserRole item)
        {
            if (item is null)
                throw new ArgumentNullException("item");


            var role = _context.UserRoles.FirstOrDefault(x => x.Id == item.Id);
            if (role != null)
            {
                var nextIndex = _context.UserRoles.OrderBy(x => x.Id).Last().Id;
                item.Id = ++nextIndex;
            }

            _context.UserRoles.Add(item);
            _context.SaveChanges();
        }

        public void AddRange(IEnumerable<UserRole> range)
        {
            if (range is null)
                throw new ArgumentNullException("range");

            foreach (var item in range)
            {
                if (_context.UserRoles.Any(x => x.Id == item.Id))
                    throw new ArgumentException("range");
            }

            _context.UserRoles.AddRange(range);
            _context.SaveChanges();
        }

        public void Update(UserRole item)
        {
            var role = _context.UserRoles.FirstOrDefault(x => x.Id == item.Id);
            if (role == null)
                throw new ArgumentException("id");
            _context.UserRoles.Update(item);
            _context.SaveChanges();
        }

        public void Delete(UserRole item)
        {
            if (item is null)
                throw new ArgumentNullException("item");

            _context.UserRoles.Remove(item);
            _context.SaveChanges();
        }

        public void DeleteById(int id)
        {
            var role = _context.UserRoles.FirstOrDefault(x => x.Id == id);
            if (role == null)
                throw new ArgumentException("user");

            _context.UserRoles.Remove(role);
            _context.SaveChanges();
        }

        public IEnumerable<UserRole> GetItems()
        {
            return _context.UserRoles;
        }

        public IQueryable<UserRole> GetItemsQueryable()
        {
            return _context.UserRoles;
        }

    }
}
