﻿using AutoMapper;
using DAL.Context;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL.Repository
{
    public class ProjectRepository : IRepository<Project>
    {
        private readonly InitialDataContext _context;
        public ProjectRepository(InitialDataContext context)
        {
            _context = context;
        }
        public void Add(Project item)
        {
            if (item is null)
                throw new ArgumentNullException("item");

            var project = _context.Projects.FirstOrDefault(x => x.Id == item.Id);
            if (project != null)
            {
                var nextIndex = _context.Projects.OrderBy(x => x.Id).Last().Id;
                item.Id = ++nextIndex;
            }

            _context.Projects.Add(item);
             _context.SaveChanges();
        }

        public void AddRange(IEnumerable<Project> range)
        {
            if (range is null)
                throw new ArgumentNullException("range");

            foreach(var item in range)
            {
                if (_context.Projects.Any(x => x.Id == item.Id))
                    throw new ArgumentException("range");
            }

            _context.Projects.AddRange(range);
            _context.SaveChanges();
        }

        public void Delete(Project item)
        {
            if (item is null)
                throw new ArgumentNullException("item");

            _context.Projects.Remove(item);
            _context.SaveChanges();
        }

        public void DeleteById(int id)
        {
            var project = _context.Projects.FirstOrDefault(x => x.Id == id);
            if (project == null)
                throw new ArgumentException("id");

            _context.Projects.Remove(project);
            _context.SaveChanges();
        }

        public Project GetItem(int id)
        {
            var project = _context.Projects.FirstOrDefault(x => x.Id == id);
            if (project == null)
                throw new ArgumentException("id");

            return project;
            
        }

        public IEnumerable<Project> GetItems()
        {
            return _context.Projects;
        }

        public IQueryable<Project> GetItemsQueryable()
        {
            return _context.Projects;
        }

        public void Update(Project item)
        {
            if (item is null)
                throw new ArgumentNullException("item");

            var project = _context.Projects.FirstOrDefault(x => x.Id == item.Id);
            if(project is null)
                throw new ArgumentException("item");
            _context.Projects.Update(item);
            _context.SaveChanges();
        }
    }
}
